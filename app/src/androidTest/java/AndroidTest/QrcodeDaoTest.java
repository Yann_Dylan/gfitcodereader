package AndroidTest;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import Models.AppDatabase;
import Models.Qrcode;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class QrcodeDaoTest {
    // FOR DATA
    private AppDatabase database;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void initDb() throws Exception {
        this.database = Room.inMemoryDatabaseBuilder(getApplicationContext(),
                AppDatabase.class)
                .allowMainThreadQueries()
                .build();
    }

    // DATA SET FOR TEST
    private static long USER_ID = 1;
    private static Qrcode aqrcode = new Qrcode("78945612320320","7897878798798");


    @Test
    public void insertAndGetUser() throws InterruptedException {
        // BEFORE : Adding a new user
        this.database.qrcodeDao().addQrcode(new Qrcode("78945612320320","454545"));

       // TEST
        Qrcode qrcode2 = (this.database.qrcodeDao().loadAllQrcode().get(0));
    }

    @After
    public void closeDb() throws Exception {
        database.close();
    }
}
