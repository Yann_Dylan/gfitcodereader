package Models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Qrcode {


    @NonNull
    @PrimaryKey
    private String qrcodetext ;

    private String  dateEnregistrement ;

    public Qrcode(String qrcodetext,String dateEnregistrement) {
        this.setQrcodetext(qrcodetext);
        this.setDateEnregistrement(dateEnregistrement);
    }

    public String getQrcodetext() {
        return qrcodetext;
    }

    public void setQrcodetext(String qrcodetext) {
        this.qrcodetext = qrcodetext;
    }

    public String getDateEnregistrement() {
        return dateEnregistrement;
    }

    public void setDateEnregistrement(String dateEnregistrement) {
        this.dateEnregistrement = dateEnregistrement;
    }
}

