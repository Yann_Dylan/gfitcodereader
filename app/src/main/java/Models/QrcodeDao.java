package Models;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface QrcodeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long addQrcode(Qrcode qrC);

    @Query("SELECT * FROM qrcode order by rowid desc")
    public List<Qrcode> loadAllQrcode() ;

}
