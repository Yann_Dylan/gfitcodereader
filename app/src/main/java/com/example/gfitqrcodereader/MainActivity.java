package com.example.gfitqrcodereader;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.room.Room;

import java.text.SimpleDateFormat;
import java.util.Date ;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import Models.AppDatabase;
import Models.Qrcode;

public class MainActivity extends AppCompatActivity {

    private AppDatabase db ;
    private int CAMERA_PERMISSION_CODE = 1;
    private TextView mresultat_label ;
    private  TextView mqrcode;
    private  TextView mchaine1;
    private  TextView mchaine2;
    private  TextView mchaine3;
    private  TextView mchaine4;
    private  TextView mchaine5;
    private  TextView mchaine6;
    private  TextView mchaine7;
    private  TextView mchaine8;
    private  TextView mchaine9;
    private  Button mstartScanner;
    private  Button mTable ;
    private SharedPreferences mPreferences;
    private static final String MEMORY_TABLE_NAME = "QRCODE_MEMORY" ;
    private static final int SCANNER_ACTIVITY_REQUEST_CODE = 2;
    private static final int TABLE_ACTIVITY_REQUEST_CODE = 3 ;
    private ArrayList<String> mResultat = new ArrayList<String>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (SCANNER_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            // Fetch the score from the Intent
            mResultat = data.getStringArrayListExtra(Scanner_Activity.RESULTAT_QRCODE) ;

            if (mResultat.size()!=1)
            {
                mresultat_label.setText("");

                // Sauvegarde du qrcode

                Date now = new Date () ;
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");

                String currentDate = formatter.format(now) ;


                Qrcode qrcode =  new Qrcode(mResultat.get(0),currentDate);

                db.qrcodeDao().addQrcode(qrcode) ;




                Toast.makeText(this, " Ce QrCode respecte la taille définie", Toast.LENGTH_LONG).show();
                mqrcode.setText("Code : "+mResultat.get(0));
                mchaine1.setText("01 : "+mResultat.get(1).toString());
                mchaine1.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                mqrcode.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(1).compareTo("01")!=0)
                {
                    /*getResources().getColor(R.color.grandfrais_red)*/
                    mchaine1.setText(mchaine1.getText()+" (incorrect)");
                    mchaine1.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;


                }
                mchaine2.setText("Gencod : "+mResultat.get(2));
                mchaine2.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                mchaine3.setText("0 : "+mResultat.get(3));
                mchaine3.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(3).compareTo("0")!=0)
                {
                    mchaine3.setText(mchaine3.getText()+" (incorrect)");
                    mchaine3.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }

                mchaine4.setText("17 : "+mResultat.get(4));
                mchaine4.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(4).compareTo("17")!=0)
                {
                    mchaine4.setText(mchaine4.getText()+" (incorrect)");
                    mchaine4.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }

                String jour = mResultat.get(5) ;
                String mois = mResultat.get(6) ;
                String annee = mResultat.get(7) ;

               String  date = jour+"/"+mois+"/"+annee ;

                int ijour = Integer.valueOf(jour) ;
                int imois = Integer.valueOf(mois) ;
                int iannee = Integer.valueOf(annee) ;

                if (ijour>31 || imois >12)
                {
                    mchaine5.setText("DLC : "+date+ " (incorrecte)");
                    mchaine5.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;
                }
                else
                {
                    mchaine5.setText("DLC : "+date);
                    mchaine5.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                }
                mchaine6.setText("3103 : "+mResultat.get(8));
                mchaine6.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(8).compareTo("3103")!=0)
                {
                    mchaine6.setText(mchaine6.getText()+" (incorrect)");
                    mchaine6.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }
                /*TRAITEMENT DU POIDS */
                String str7 = mResultat.get(9) ;
                String  partieE = str7.substring(0,3) ;
                String   partieD = str7.substring(3) ;
                String  number = partieE+"."+partieD ;
                double poids = Double.valueOf(number) ;

                mchaine7.setText("Poids : "+poids+" Kg");
                mchaine7.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                mchaine8.setText("10 : "+mResultat.get(10));
                mchaine8.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(10).compareTo("10")!=0)
                {
                    mchaine8.setText(mchaine8.getText()+" (incorrect)");
                    mchaine8.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;
                }

                mchaine9.setText("Numero de lot : "+mResultat.get(11));
                mchaine9.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;


            }
            else {

                mresultat_label.setText("");
                Toast.makeText(this, " Ce qrcode ne respecte pas la taille définie et ne contient pas toutes les informations utiles",Toast.LENGTH_LONG)
                        .show();
                mqrcode.setText("Code incorrecte : "+mResultat.get(0)+ "\n\n\n Ce qrcode ne respecte pas la taille définie et ne contient pas toutes les informations utiles \n\n\n Veuillez réessayer avec un bon code");
                mqrcode.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;
                mchaine1.setText("");
                mchaine2.setText("");
                mchaine3.setText("");
                mchaine4.setText("");
                mchaine5.setText("");
                mchaine6.setText("");
                mchaine7.setText("");
                mchaine8.setText("");
                mchaine9.setText("");



            }

        }

        if (TABLE_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode)
        {

           mResultat = data.getStringArrayListExtra(Table.QRCODE_INDEX) ;


                mresultat_label.setText("Vous réaffichez un qrcode déjà scanné");


                Toast.makeText(this, "Vous réaffichez un qrcode déjà scanné", Toast.LENGTH_LONG).show();
               mqrcode.setText("Code : "+mResultat.get(0));
                mchaine1.setText("01 : "+mResultat.get(1).toString());
                mchaine1.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                mqrcode.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(1).compareTo("01")!=0)
                {
                    /*getResources().getColor(R.color.grandfrais_red)*/
                  mchaine1.setText(mchaine1.getText()+" (incorrect)");
                    mchaine1.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;


                }
                mchaine2.setText("Gencod : "+mResultat.get(2));
                mchaine2.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                mchaine3.setText("0 : "+mResultat.get(3));
                mchaine3.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(3).compareTo("0")!=0)
                {
                    mchaine3.setText(mchaine3.getText()+" (incorrect)");
                    mchaine3.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }

                mchaine4.setText("17 : "+mResultat.get(4));
                mchaine4.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(4).compareTo("17")!=0)
                {
                    mchaine4.setText(mchaine4.getText()+" (incorrect)");
                    mchaine4.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }

                String jour = mResultat.get(5) ;
                String mois = mResultat.get(6) ;
                String annee = mResultat.get(7) ;

                String  date = jour+"/"+mois+"/"+annee ;

                int ijour = Integer.valueOf(jour) ;
                int imois = Integer.valueOf(mois) ;
                int iannee = Integer.valueOf(annee) ;

                if (ijour>31 || imois >12)
                {
                    mchaine5.setText("DLC : "+date+ " (incorrecte)");
                    mchaine5.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;
                }
                else
                {
                    mchaine5.setText("DLC : "+date);
                    mchaine5.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;
                }
                mchaine6.setText("3103 : "+mResultat.get(8));
                mchaine6.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(8).compareTo("3103")!=0)
                {
                    mchaine6.setText(mchaine6.getText()+" (incorrect)");
                    mchaine6.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;

                }
                /*TRAITEMENT DU POIDS */
               String str7 = mResultat.get(9) ;
                String  partieE = str7.substring(0,3) ;
                String   partieD = str7.substring(3) ;
                String  number = partieE+"."+partieD ;
                double poids = Double.valueOf(number) ;

                mchaine7.setText("Poids : "+poids+" Kg");
                mchaine7.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                mchaine8.setText("10 : "+mResultat.get(10));
                mchaine8.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;

                if (mResultat.get(10).compareTo("10")!=0)
                {
                    mchaine8.setText(mchaine8.getText()+" (incorrect)");
                    mchaine8.setTextColor(getResources().getColor(R.color.grandfrais_red)) ;
                }

                mchaine9.setText("Numero de lot : "+mResultat.get(11));
                mchaine9.setTextColor(getResources().getColor(R.color.colorPrimaryDark)) ;


            }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mresultat_label =(TextView) findViewById(R.id.resultat_label) ;
        mqrcode = (TextView)  findViewById(R.id.qrcode);
        mstartScanner = (Button) findViewById(R.id.scanner);
        mTable = (Button) findViewById(R.id.table) ;
        mchaine1 = (TextView)  findViewById(R.id.chaine1);
        mchaine2 = (TextView)  findViewById(R.id.chaine2);
        mchaine3 = (TextView)  findViewById(R.id.chaine3);
        mchaine4 = (TextView)  findViewById(R.id.chaine4);
        mchaine5 = (TextView)  findViewById(R.id.chaine5);
        mchaine6 = (TextView)  findViewById(R.id.chaine6);
        mchaine7 = (TextView)  findViewById(R.id.chaine7);
        mchaine8 = (TextView)  findViewById(R.id.chaine8);
        mchaine9 = (TextView)  findViewById(R.id.chaine9);

        // Initialisation de la base de données

        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"QrcodeDatabase")
                .allowMainThreadQueries()
                .build();

        mstartScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    Intent scannerActivityIntent = new Intent(MainActivity.this, Scanner_Activity.class);
                    startActivityForResult(scannerActivityIntent, SCANNER_ACTIVITY_REQUEST_CODE );

                } else {
                    requestStoragePermission();
                }

            }
        });

        mTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent tableActivityIntent = new Intent(MainActivity.this, Table.class);
                    startActivityForResult(tableActivityIntent, TABLE_ACTIVITY_REQUEST_CODE);



            }
        });



    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {

            new AlertDialog.Builder(this)
                    .setTitle("Permission refusée")
                    .setMessage("Cette permission est nécessaire car sans la camera nous ne pouvons scanner de QRcode")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("annuler", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission accordée", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Permission refusée", Toast.LENGTH_LONG).show();
            }
        }
    }
}
