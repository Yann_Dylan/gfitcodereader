package com.example.gfitqrcodereader;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.Result;

import java.util.ArrayList;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Scanner_Activity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    ZXingScannerView scannerView ;
    public static final String RESULTAT_QRCODE = "RESULTAT_QRCODE";
    private ArrayList<String> mResultat = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scannerView = new ZXingScannerView(this) ;
        setContentView(scannerView);
        
    }



    @Override
    public void handleResult(Result result) {

        String numeroQr = result.getText() ;
        int length = numeroQr.length() ;
        if (length==45)
        {
            String str1,str2,str3,str4,str5,str6,str7,str8,str9 ;
            double poids ;
            String date,jour,mois,annee,partieE,partieD,number;

            str1 = numeroQr.substring(0,2) ;
            str2 = numeroQr.substring(2,15) ;
            str3 = numeroQr.substring(15,16) ;
            str4 = numeroQr.substring(16,18) ;
            str5 = numeroQr.substring(18,24) ;
            str6 = numeroQr.substring(24,28) ;
            str7 = numeroQr.substring(28,34) ;
            str8 = numeroQr.substring(34,36) ;
            str9 = numeroQr.substring(36) ;

            // TRAITEMENT DES INFORMATIONS

                // Traitement de la date
                    annee = str5.substring(0,2) ;
                    mois=  str5.substring(2,4);
                    jour= str5.substring(4) ;

                    date = jour+"/"+mois+"/"+annee ;

                // Traitement du poids

                partieE = str7.substring(0,3) ;
                partieD = str7.substring(3) ;
                number = partieE+"."+partieD ;
                poids = Double.valueOf(number) ;


             // REMPLISSAGE DE LA VARIABLE RESULTAT

                mResultat.add(numeroQr) ; //0
                mResultat.add(str1) ; // 1
                mResultat.add(str2) ; //2
                mResultat.add(str3) ; // 3
                mResultat.add(str4) ; // 4
                mResultat.add(jour) ; // 5
                mResultat.add(mois) ; // 6
                mResultat.add(annee) ; // 7
                mResultat.add(str6) ; // 8
                mResultat.add(str7) ; // 9
                mResultat.add(str8) ; // 10
                mResultat.add(str9) ; // 11

                        }
        else
        {
            mResultat.add(numeroQr);

        }
        Intent intent = new Intent();
        intent.putExtra(RESULTAT_QRCODE,mResultat) ;
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }


}
