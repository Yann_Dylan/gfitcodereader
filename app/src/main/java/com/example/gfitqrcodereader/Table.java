package com.example.gfitqrcodereader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import Models.AppDatabase;
import Models.Qrcode;

public class Table extends AppCompatActivity implements View.OnClickListener{

    private AppDatabase db ;
    public static final String QRCODE_INDEX="QRCODE_INDEX" ;
    private ArrayList<String> mResultat = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"QrcodeDatabase")
                .allowMainThreadQueries()
                .build();
        List<Qrcode> qrCodes = this.db.qrcodeDao().loadAllQrcode() ;
        int qrCodesLength = qrCodes.size() ;
        LinearLayout tableLayout = (LinearLayout) findViewById(R.id.tableLayout);

        for (int i=0; i<qrCodesLength ; i++)
        {
            Qrcode qrCode = qrCodes.get(i);
            Button button = new Button(this) ;
            button.setText("Qrcode : "+qrCode.getQrcodetext()+"\n\n scanné le "+qrCode.getDateEnregistrement());
            button.setPadding(10,0,10,0);

                button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


            button.setTextColor(getResources().getColor(R.color.white));
            button.setTag(qrCode.getQrcodetext());
            button.setOnClickListener(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(10,10,10,10);
            lp.gravity= Gravity.CENTER ;

            tableLayout.addView(button,lp);

        }

    }

    @Override
    public void onClick(View v) {

        String numeroQr = (String) v.getTag() ;
        int length = numeroQr.length() ;
        if (length==45)
        {
            String str1,str2,str3,str4,str5,str6,str7,str8,str9 ;
            String jour,mois,annee ;

            str1 = numeroQr.substring(0,2) ;
            str2 = numeroQr.substring(2,15) ;
            str3 = numeroQr.substring(15,16) ;
            str4 = numeroQr.substring(16,18) ;
            str5 = numeroQr.substring(18,24) ;
            str6 = numeroQr.substring(24,28) ;
            str7 = numeroQr.substring(28,34) ;
            str8 = numeroQr.substring(34,36) ;
            str9 = numeroQr.substring(36) ;

            // TRAITEMENT DES INFORMATIONS

            // Traitement de la date
            annee = str5.substring(0,2) ;
            mois=  str5.substring(2,4);
            jour= str5.substring(4) ;


            // REMPLISSAGE DE LA VARIABLE RESULTAT

            mResultat.add(numeroQr) ; //0
            mResultat.add(str1) ; // 1
            mResultat.add(str2) ; //2
            mResultat.add(str3) ; // 3
            mResultat.add(str4) ; // 4
            mResultat.add(jour) ; // 5
            mResultat.add(mois) ; // 6
            mResultat.add(annee) ; // 7
            mResultat.add(str6) ; // 8
            mResultat.add(str7) ; // 9
            mResultat.add(str8) ; // 10
            mResultat.add(str9) ; // 11

        Intent intent = new Intent();
        intent.putExtra(QRCODE_INDEX,mResultat) ;
        setResult(RESULT_OK, intent);
        finish();

    }
}}
